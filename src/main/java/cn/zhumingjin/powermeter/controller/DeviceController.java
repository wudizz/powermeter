package cn.zhumingjin.powermeter.controller;

import cn.zhumingjin.powermeter.controller.vo.DeviceVo;
import cn.zhumingjin.powermeter.controller.vo.TableVo;
import cn.zhumingjin.powermeter.entity.Device;
import cn.zhumingjin.powermeter.service.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class DeviceController {

    @Autowired
    public DeviceService deviceService;

    @RequestMapping(path = "/getAllDevices", method = RequestMethod.POST)
    public TableVo getDevices() {
        System.out.println("/getAllDevices");
        List<Device> list = deviceService.getDevices();
        ArrayList<DeviceVo> result = new ArrayList<>();
        for (Device device : list)
            result.add(new DeviceVo(device.getId(), device.getName(),
                    device.getType(), device.getDescription()));

        return new TableVo(0, "", result.size(), result);
    }
}
