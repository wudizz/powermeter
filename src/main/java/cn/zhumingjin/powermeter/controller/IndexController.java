package cn.zhumingjin.powermeter.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {

    @RequestMapping(path = "/")
    public String index(){
        return "forward:index.html";
    }

    @RequestMapping(path = "/loginView")
    public String loginView(){
        return "forward:login.html";
    }
}
