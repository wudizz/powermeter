package cn.zhumingjin.powermeter.controller.vo;

import lombok.Data;

@Data
public class UserVo {
    public int id;
    public String name;
    public String password;
    public int privilege;


    public UserVo(Integer id, String name, String password, Integer privilege) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.privilege = privilege;
    }
}
