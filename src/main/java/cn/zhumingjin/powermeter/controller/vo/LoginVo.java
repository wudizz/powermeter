package cn.zhumingjin.powermeter.controller.vo;

import lombok.Data;

@Data
public class LoginVo {
    private Integer code; // 0 登录成功， 1 用户id或密码错误

    private Integer id;

    private Integer privilege;

    private String name;

    public LoginVo(Integer code, Integer id, Integer privilege, String name) {
        this.code = code;
        this.id = id;
        this.privilege = privilege;
        this.name = name;
    }
}
