package cn.zhumingjin.powermeter.controller.vo;

import java.util.List;

public class LayUIVo<T> {
    public int code;
    public String msg;

    public int count;

    public List<T> data;

    public LayUIVo(int code, String msg, int count, List<T> data) {
        this.code = code;
        this.msg = msg;
        this.count = count;
        this.data = data;
    }
}
