package cn.zhumingjin.powermeter.controller.vo;

import java.util.List;

public class TableVo {
    public int code;
    public String msg;

    public int count;

    public TableVo(int code, String msg, int count, List<DeviceVo> data) {
        this.code = code;
        this.msg = msg;
        this.count = count;
        this.data = data;
    }

    public List<DeviceVo> data;
}
