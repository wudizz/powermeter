package cn.zhumingjin.powermeter.controller.vo;

import lombok.Data;

@Data
public class DeviceVo {
    public int id;
    public String name;
    public int type;
    public String description;

    public DeviceVo(int id, String name, int type, String description) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.description = description;
    }
}
