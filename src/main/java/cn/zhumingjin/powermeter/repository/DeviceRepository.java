package cn.zhumingjin.powermeter.repository;

import cn.zhumingjin.powermeter.entity.Device;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeviceRepository extends JpaRepository<Device, Integer> {
}
