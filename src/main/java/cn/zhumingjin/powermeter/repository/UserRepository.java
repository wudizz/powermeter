package cn.zhumingjin.powermeter.repository;

import cn.zhumingjin.powermeter.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User,Integer> {

    User findByIdAndPassword(Integer id, String password);

//    public User getUserByName(User user);
//
//    public int addUser(User user);

}
