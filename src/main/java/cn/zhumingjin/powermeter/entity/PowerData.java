package cn.zhumingjin.powermeter.entity;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;

import java.sql.Time;
import java.util.ArrayList;

@Data
@Entity
@Table(name = "power_data")
public class PowerData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @GenericGenerator(name = "myIdStrategy", strategy = "uuid")
    @Column(name = "id")
    private Integer id;

    @Basic
    @Column(name = "time")
    @CreatedDate
    private Time time;

    @Basic
    @Column(name = "voltage")
    private String voltageURL;

    @Basic
    @Column(name = "current")
    private String currentURL;

    @Basic
    @Column(name = "sampleRate")
    private Double sampleRate;

    @Basic
    @Column(name = "length")
    private Integer length;

    @ManyToOne(targetEntity = Device.class)
    @JoinColumn(name = "device", referencedColumnName = "id")
    private Device device;
}
