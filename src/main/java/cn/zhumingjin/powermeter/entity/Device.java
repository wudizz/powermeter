package cn.zhumingjin.powermeter.entity;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

@Data
@Entity
@Table(name = "device")
public class Device {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @GenericGenerator(name = "myIdStrategy", strategy = "uuid")
    @Column(name = "id")
    private Integer id;

    @Basic
    @Column(name = "name")
    private String name;

    /**
     * 物联网设备类型 0 边缘层电表网关
     */
    @Basic
    @Column(name = "type")
    private Integer type = 0;

    @Basic
    @Column(name = "decription")
    private String description;
}
