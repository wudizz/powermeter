package cn.zhumingjin.powermeter.entity;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

@Data
@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Basic
    @Column(name = "name")
    private String name;

    @Basic
    @Column(name = "password", nullable = false)
    private String password;

    /**
     * 用户权限 0 管理员 1 普通用户
     */
    @Basic
    @Column(name = "privilege")
    private Integer privilege = 0;
}
