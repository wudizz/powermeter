package cn.zhumingjin.powermeter.service;

import cn.zhumingjin.powermeter.entity.User;
import cn.zhumingjin.powermeter.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    public UserRepository userRepository;

    public List<User>  getUsers(){return  userRepository.findAll();};

    public User login(Integer id, String password){
        return userRepository.findByIdAndPassword(id, password);
    }
}
