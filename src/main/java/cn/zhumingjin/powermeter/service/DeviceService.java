package cn.zhumingjin.powermeter.service;

import cn.zhumingjin.powermeter.entity.Device;
import cn.zhumingjin.powermeter.repository.DeviceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DeviceService {

    @Autowired
    public DeviceRepository deviceRepository;

    public List<Device> getDevices() {
        return deviceRepository.findAll();
    }
}
